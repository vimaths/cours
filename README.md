# Cours de Victoire Hérin

## Lancer un apercu

`python3 -m venv .venv`

`source .venv/bin/activate`

`pip3 install -r requirements.txt`

`mkdocs serve`

## Documentation

La documentation pour utiliser ce modèle est sur [tutoriels](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/)

En particulier : [tutoriels création de site](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/08_tuto_fork/1_fork_projet/)


Pour signaler un souci sur le **modèle**, écrire à [forge-apps+guichet+modeles-projets-mkdocs-pyodide-review-99-issue-@phm.education.gouv.fr](mailto:forge-apps+guichet+modeles-projets-mkdocs-pyodide-review-99-issue-@phm.education.gouv.fr) ou ouvrir un ticket sur [tickets](https://forge.apps.education.fr/modeles-projets/mkdocs-simple-review/-/issues)


