# Les mathématiques, c’est <span id="ique">fantastique</span>

Sur ce site se trouvent mes traces écrites de cours (avec quelques remarques pour moi), les fiches d’exercices, des corrections, et des curiosités mathématiques.

## Liens cools en rapport avec les mathématiques

- [QRcode vers ce site](vimaths.png)
- [Toute une page de QRcodes vers ce site](telecharger/qrcodevimaths.pdf)
- [Mes vidéos !](https://tube-sciences-technologies.apps.education.fr/a/herin_victoire/video-channels)
- [Blog d’Arnaud et Julien Durand, professeurs de mathématiques](https://mathix.org/linux/)
- [m@ths et tiques, chaine d’Yvan Monka, professeur de mathématiques](https://www.youtube.com/@YMONKA)
- [Les manuels Sésamath, libres et gratuits, qui me servent de base quand je manque d’inspiration pour un cours ou un exercice](https://manuel.sesamath.net/)
- [DicoNombres, un site rempli de curiosités mathématiques](http://villemin.gerard.free.fr/NombDico/index.htm)
- [Plug-in Libre Office du Cartable fantastique, pour facilement écrire des mathématiques sur Libre Office !](https://www.cartablefantastique.fr/outils-pour-compenser/le-plug-in-libre-office/)

## Liens cools moins en rapport avec les mathématiques

- [Lofi Girl, chaine de musique idéale pour étudier et travailler](https://www.youtube.com/@LofiGirl")
- [LaTeX, le langage de balisage que j’utilise pour rédiger mes cours et exercices (LA meilleure manière d’écrire des mathématiques sur un ordinateur)](https://www.latex-project.org/)
- [Overleaf, éditeur et compilateur LaTeX en ligne, que j’utilise surtout pour ses fiches de rappel](https://www.overleaf.com/)
- [Le Wikitionnaire, dictionnaire collaboratif, complet et descriptiviste](https://fr.wiktionary.org)
- [Apprendre la méthode Roux pour résoudre un Rubik’s cube (en anglais)](https://www.youtube.com/playlist?list=PLBHocHmPzgIjnAbNLHDycgaCP5IqiwnU9)
- [Lichess, excellent site pour apprendre et jouer aux échecs](https://lichess.org/)

<script type="text/javascript">
    var adjectifs = [
        "fantastique",
        "logique",
        "(pas) magique",
        "bénéfique",
        "(pas) chaotique",
        "charismatique",
        "épique",
        "esthétique",
        "exotique",
        "héroïque",
        "iconique",
        "magnifique",
        "mathématique",
        "linguistique",
        "tétrasyllabique",
        "poétique",
        "quadratique",
        "symétrique",
        "automatique",
        "sympathique",
        "vitaminique",
        "(ça fait travailler les) zygomatiques"];
    var aleatoire_entre_0_et_1 = Math.random();
    var nombre_adjectifs = adjectifs.length;
    var nombre_aleatoire = aleatoire_entre_0_et_1 * nombre_adjectifs;
    var arrondi = Math.floor(nombre_aleatoire);
    var adjectif_choisi =  adjectifs[arrondi];
    var tag = document.getElementById('ique');
    tag.innerHTML = adjectif_choisi;
</script>