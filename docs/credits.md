---
author: Victoire Hérin
title: Crédits
---

Ce site est hébergé par [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/).

Je remercie les collègues d’informatique et leur [Association des Enseignants d’Informatique de France](https://aeif.fr/index.php/accueil/), qui est à l’origine du projet de forge.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Tout ce qui se trouve sur ce site, sauf mention contraire, est CC BY-SA ; que ça soit par choix parce que je suis l’autrice du contenu, ou par « contrainte » car il est sourcé d’ailleurs.



