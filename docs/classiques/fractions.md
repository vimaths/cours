---
author: Victoire Hérin
title: Fractions
---

## Définitions

!!! info "Définition d’une fraction"
    Soient $a$ et $b$ deux nombres **entiers**, avec $b$ non nul.
    La **fraction** $\dfrac{a}{b}$ est le **nombre** qui, multiplié par $b$, donne $a$.  
    $\dfrac{a}{b} \times b = a$

!!! warning
    À noter que les fractions sont aussi appelées nombres rationnels.

??? note "Pourquoi ?"
    Parce qu’elles représentes un ratio, une « division » entre 2 nombres.

!!! example "Exemples"
    - La fraction $\dfrac{2}{3}$ est le nombre qui, multiplié par 3, donne 2. $\dfrac{2}{3} \times 3 = 2$.
    - La fraction $\dfrac{7}{4}$ est le nombre qui, multiplié par 4, donne 7. $\dfrac{7}{4} \times 4 = 7$.

!!! info "Vocabulaire d’une fraction"
    Soit une fraction $\dfrac{a}{b}$.  
    Le nombre $a$ est appelé le **numérateur**, et le nombre $b$ s’appelle le **dénominateur**.  
    Le symbole entre les 2 nombres est la **barre de fraction**.

??? note "Pourquoi ?"
    D’après [Wikipedia](https://fr.wikipedia.org):
    - « [Numérateur](https://fr.wikipedia.org/wiki/Num%C3%A9rateur) » vient du mot latin numerator, qui signifie « celui qui compte ».
    - [Dénominateur](https://fr.wikipedia.org/wiki/D%C3%A9nominateur) : emprunté du latin « denominator », qui signifie « celui qui désigne », c'est donc celui qui décide, celui qui indique « par combien on partage ».

## Propriétés

!!! info "Fractions égales"
    Soit la fraction $\dfrac{a}{b}$ et un nombre $k$ non nul. On a alors :  
    $\dfrac{a}{b} = \dfrac{a \times k}{b \times k} = \dfrac{a \div k}{b \div k}$

!!! info "Simplifier une fraction"
    Lorsque l’on « réduit » (rapproche de 0) le **dénominateur** d’une fraction, on dit qu’on la simplifie.

!!! example "Exemple"
    Simplifier $\dfrac{4}{6}$.  
    $\dfrac{4}{6} = \dfrac{4 \div 2}{6 \div 2} = \dfrac{2}{3}$

??? note "Fraction irréductible"
    Lorsque qu’une fraction est simplifiée au maximum (qu’il n’y a plus de diviseurs communs entre son numérateur et son dénominateur), on dit qu’elle est **irréductible** (comme les gaulois).

## Bonus

- [Playlist d’Yvan Monka sur les fractions en 5ᵉ](https://www.youtube.com/watch?v=m0HUFgWbgsA&list=PLVUDmbpupCaorU_NqVot4wsX1uyOIEKeG)
- [Playlist d’Yvan Monka sur les fractions en 4ᵉ](https://www.youtube.com/watch?v=a0Qb812W75c&list=PLVUDmbpupCarGS9399o4YIbb88_ytmvpc)