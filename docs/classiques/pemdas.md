---
author: Victoire Hérin
title: Priorité des opérations
---

1. Parenthèses de l’intérieur vers l’extérieur
2. Exposants (puissances) (vu en 4ᵉ)
3. Multiplications et divisions, de gauche à droite
4. Additions et soustractions, de gauche à droite